# Set up a basic addok demo

1. Reproject the Presov City data to EPSG:4326 (using QGIS)
1. cd into this folder 
    ```
    cd ~/dev/sk/geocoder
    ```
1. Transform the data (run the presov_city_open_data_2_addok_sjson.py in pycharm)
    ```
    ./formating_scripts/venv/bin/python ./formating_scripts/presov_city_open_data_2_addok_sjson.py data/addok_source/presov_city_adresy/adresy_epsg4326.geojson data/addok_source/presov_city_adresy/presov_city_adresy.sjson
    
    ```
1. run a redis instance
```
docker run -v ./data/redis_data:/data -p 6379:6379 --name addok-redis -d redis redis-server --appendonly yes
```
1. activate python virtualenv 
    ```
    cd ~/dev/sk/geocoder
    source .addok.venv/bin/activate
    ``` 
1. Load data into addok
    ```
    # reset if necessary
    addok reset
    addok batch data/addok_source/presov_city_adresy/presov_city_adresy.sjson
    addok ngrams
   ```
   
1. Run dev server
    ```
    addok serve
    ``` 
 1. Open in browser 
    * http://127.0.0.1:7878/search/?q=10,%20Pal (geocoding)
    * http://127.0.0.1:7878/reverse/?lon=21.2146&lat=49.010 (reverse)